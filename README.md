# MIT Starter Quest
This is a [Solarus Quest](https://www.solarus-games.org/) which cantains only [MIT](https://en.wikipedia.org/wiki/MIT_License) licensed scripts and [Public Domain](https://creativecommons.org/publicdomain/zero/1.0/) art assets. [Here](Art_Asset_List.txt) is the list of art assets used.
