-- This class handles all elements on screen during a dialog.
-- (dialog_box, name_box, sprites, sounds, etc)
-- 
-- Usage:
--  Require it: 
--    dialog_manager = require('scripts/dialogs/dialog_manager')
--  Start it:
--    dialog_manager:create(game)

local visual_novel_manager = {}

require("scripts/dialogs/conversation_manager")

require("scripts/dialogs/libs/config_loader")
require("scripts/dialogs/libs/table_helpers")

function visual_novel_manager:create(game)
  -- Creates a scene object
  scene_manager = require("scripts/dialogs/scene_manager"):create(game)

  -- Called by the engine when a dialog finishes. Shouldn't be called by directly by you!
  --
  -- dialog - A table containg the dialog information to display
  --
  -- Example
  --   N/A
  --
  -- Returns nothing
  function game:on_dialog_started(dialog, info)
    conversation = conversation_parser(dialog, info)
    
    delay = scene_manager:transition_delay('exit')
    transition('exit')
    sol.timer.start(game, delay + 10, function() -- gave an extra 10 miliseconds for scene to wrap up past it's close
      scene_manager:set_scene(conversation, load_scene(conversation))
      sol.menu.start(visual_novel_manager, scene_manager)
    end)
  end

  -- Called by the engine when a dialog finishes. Shouldn't be called by directly by you!
  --
  -- dialog - A table containg the dialog information that was just displayed
  --
  -- Example
  --   N/A
  --
  -- Returns nothing
  function game:on_dialog_finished(dialog)
    sol.menu.stop(scene_manager)
  end
end

-- Loads all the information for the scene manager to do his job
-- e.g. which background, characters, transitions, etc
-- Priority is conversation > scene_config > character_config
--
-- Which means that you CAN specify character attributes (sprites and the like) in the
-- scene config and that will overwrite the character config
--
-- conversation - The raw dialog table that Solarus gives us. We use it's attributes
--                to determine what configs to load and what to populate them with
-- 
-- Example:
--   load_scene({ 
--     'name' = 'Todd', 
--     'dialog_id' = 'todd.greeting',  
--     ... 
--   })
--   #=> { 
--    'background' = 'starry_night', 
--    'characters' = { CHARACTER CONFIGURATIONS }, 
--    ... 
--   }
--
-- Returns a table containg the config that the scene will use.
function load_scene(conversation)
  scene_config = {}
  scene_config['characters'] = load_default_character_configs(get_unique_speakers(conversation))
  scene_config = recursive_merge(scene_config, load_scene_config(conversation.id))

  return scene_config
end

return visual_novel_manager
