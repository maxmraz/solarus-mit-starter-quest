local initial_game = require("scripts/initial_game")
local vn_manager = require('scripts/dialogs/visual_novel_manager')

local game_manager = {}

-- Creates the game
--
-- file - A string that contains the Name of the file
--
-- Example
--  game_manager:create(file => string)
--
-- Returns nothing
function game_manager:create(file)
  local exists = sol.game.exists(file)
  local game = sol.game.load(file)
  if not exists then
    initial_game:initialize_new_savegame(game)
  end

  function game:on_started()
    vn_manager:create(game)
    sol.menu.start(game, vn_manager)
  end

  return game
end

function game_manager:on_finished()
  sol.menu.stop(vn_manager)
end

return game_manager