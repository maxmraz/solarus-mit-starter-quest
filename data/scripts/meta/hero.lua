local hero_meta = sol.main.get_metatable("hero")

-- Function of Hero activation for this game
--
-- game - sol.game
--
-- Example 
--  initialize_hero_features(sol.game)
--
-- Returns Nothing
local function initialize_hero_features(game)
  local hero = game:get_hero()
  hero:set_tunic_sprite_id("hero/tunic1")
end